import { ObjectSchema, ValidationLocalization, validate } from "./validation";

export type Configurations = {
  defaultFetchLimit: number;
  offsetKey: string;
  limitKey: string;
  responseTransformer: (response: object | Array<object> | null | boolean) => object;
  localization: ValidationLocalization;
};

export const defaultConfigurations: Configurations = {
  defaultFetchLimit: 15,
  limitKey: "limit",
  offsetKey: "offset",
  localization: {
    requiredMessage: (key) => `"${key}" is required`,
    typeMismatchMessage: (key, value, currentType, targetType) =>
      `"${key}" should have the type of ${targetType} but ${currentType} found`,
    minViolationError: (key, value, min) => `"${key}" should have be at least ${min}`,
    maxViolationError: (key, value, max) => `"${key}" should have the at max ${max}`,
  },
  responseTransformer: (response) => ({
    data: response,
    success: true,
  }),
};
