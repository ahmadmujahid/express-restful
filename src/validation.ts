export type ObjectSchema = {
  [key: string]: {
    type: "string" | "number" | "boolean";
    isRequired?: boolean;
    requiredAt?: Array<"create" | "update">;
    min?: number;
    max?: number;
    trim?: boolean;
    transformer?: (value: any) => Promise<any> | any;
    customValidator?: (
      value: any,
    ) => { result: boolean; error?: string } | Promise<{ result: boolean; error?: string }>;
  };
};

export type ValidationLocalization = {
  requiredMessage: (key: string) => string;
  typeMismatchMessage: (key: string, value: any, currentType: string, targetType: string) => string;
  minViolationError: (key: string, value: any, min: number) => string;
  maxViolationError: (key: string, value: any, max: number) => string;
};

export const isString = (value: any) => typeof value === "string";

export const isNumber = (value: any) => typeof value === "number";

export const valueExists = (value: any) => value !== null && value !== undefined && value !== "";

export const getType = (value: any) => typeof value;

export const isBoolean = (value: any) => typeof value === "boolean";

export const validate = async (
  body: any,
  schema: ObjectSchema,
  operation: "create" | "update",
  localization: ValidationLocalization,
): Promise<{ isValid: boolean; errors: Array<string> | null; sanitizedObject: any | null }> => {
  const errors: Array<string> = [];
  const sanitizedObject: any = {};

  await Promise.all(
    Object.keys(schema).map(async (key) => {
      const { type, isRequired, requiredAt, min, max, customValidator, trim, transformer } = schema[key];
      const currentValue = body[key];
      if (isRequired && requiredAt?.find((item) => item === operation) && !valueExists(currentValue)) {
        errors.push(localization.requiredMessage(key));
        return;
      }
      if (valueExists(currentValue)) {
        if (
          (type === "string" && !isString(currentValue)) ||
          (type === "number" && !isNumber(currentValue)) ||
          (type === "boolean" && !isBoolean(currentValue))
        ) {
          errors.push(localization.typeMismatchMessage(key, currentValue, getType(currentValue), type));
          return;
        }

        if (valueExists(min)) {
          if (
            min !== undefined &&
            ((type === "string" && (currentValue as string).length < min) ||
              (type === "number" && (currentValue as number) < min))
          ) {
            errors.push(localization.minViolationError(key, currentValue, min));
            return;
          }
        }

        if (valueExists(max)) {
          if (
            max !== undefined &&
            ((type === "string" && (currentValue as string).length > max) ||
              (type === "number" && (currentValue as number) < max))
          ) {
            errors.push(localization.maxViolationError(key, currentValue, max));
            return;
          }
        }

        if (customValidator) {
          const { result, error } = await customValidator(currentValue);
          if (!result) {
            errors.push(error as string);
            return;
          }
        }
        sanitizedObject[key] = typeof currentValue === "string" && trim ? currentValue.trim() : currentValue;
        if (transformer) {
          sanitizedObject[key] = await transformer(sanitizedObject[key]);
        }
      }
    }),
  );

  return { isValid: errors.length === 0, sanitizedObject, errors };
};
