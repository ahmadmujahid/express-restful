import express, { Request } from "express";
import { ObjectSchema, validate } from "./validation";
import { Configurations, defaultConfigurations } from "./configurations";

export * from "./configurations";
export * from "./validation";

export type PaginationObject = { limit: number; offset: number };

export const defaultMiddleware = (req: any, res: any, next: any) => next();
export interface Service {
  getAll(params: object, pagination: PaginationObject): Promise<Array<object>> | Array<object>;
  getById(id: string): Promise<object | null> | object | null;
  add(body: object): Promise<object> | object;
  update(id: string, body: object): Promise<object> | object;
  delete(id: string): Promise<null> | null | void;
}

export const getPaginationParams = (req: Request, configurations: Configurations): PaginationObject => {
  const { defaultFetchLimit } = configurations;
  const { [configurations.limitKey]: limit, [configurations.offsetKey]: offset } = req.query;
  return {
    limit: limit ? Number(limit) : defaultFetchLimit,
    offset: offset ? Number(offset) : 0,
  };
};

export default (app: express.Application, userConfig: Partial<Configurations> = {}) => ({
  add: (
    url: string,
    schema: ObjectSchema,
    service: Service,
    middlewares: Partial<{ get: any; post: any; put: any; delete: any }> = {},
  ) => {
    const configurations: Configurations = {
      ...defaultConfigurations,
      ...userConfig,
    };
    app.get(url, middlewares.get || defaultMiddleware, async (req, res, next) => {
      const paginationObject = getPaginationParams(req, configurations);
      try {
        res.send(configurations.responseTransformer(await service.getAll(req.query, paginationObject)));
      } catch (e) {
        next(e);
      }
    });

    app.get(`${url}/:id`, middlewares.get || defaultMiddleware, async (req, res, next) => {
      try {
        res.send(configurations.responseTransformer(await service.getById(req.params.id as string)));
      } catch (e) {
        next(e);
      }
    });

    app.post(url, middlewares.post || defaultMiddleware, async (req, res, next) => {
      try {
        const { isValid, errors, sanitizedObject } = await validate(
          req.body,
          schema,
          "create",
          configurations.localization,
        );
        if (!isValid) {
          next({ isValidation: true, errors });
          return;
        }
        res.send(
          configurations.responseTransformer(await service.update(req.params.id as string, sanitizedObject)),
        );
      } catch (e) {
        next(e);
      }
    });

    app.put(`${url}/:id`, middlewares.put || defaultMiddleware, async (req, res, next) => {
      try {
        const { isValid, errors, sanitizedObject } = await validate(
          req.body,
          schema,
          "update",
          configurations.localization,
        );
        if (!isValid) {
          next({ isValidation: true, errors });
          return;
        }
        res.send(
          configurations.responseTransformer(await service.update(req.params.id as string, sanitizedObject)),
        );
      } catch (e) {
        next(e);
      }
    });

    app.delete(`${url}/:id`, middlewares.delete || defaultMiddleware, async (req, res, next) => {
      try {
        await service.delete(req.params.id as string);
        res.send(configurations.responseTransformer(null));
      } catch (e) {
        next(e);
      }
    });
  },
});
