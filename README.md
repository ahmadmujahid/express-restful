# express-restful

A Javascript library that integrate with express to make building restful web apps easier

## What This Library Solves ?

This library provides a simple solution to deal with the redundant code in the most common nodeJS apps with a uniform solution

In each NodeJS app we have to create a router for each route , each router will have a GET POST PUT DELETE operations and each of those method will have some kind of processing for req.params , req.query , req.body that is uniformed all over the app

## Initializing express-restful Instance

express-restful exposes a default function that takes your express ap instance along with an optional configurations you can pass , the function will return an express-restful instance

```javascript
import express from "express";
import ExpressRestful from "express-restful";

const app = express();

const expressRestful = ExpressRestful(app, {});

app.listen(3000);
```

## What Is A Service ?

A service is a set of methods that allows to perform CRUD operations on a specific type

### Typescript

A service in typescript is a class that implements our service interface

```typescript
import { Service } from "./dist";

class UsersService implements Service {
  getAll(params: object, pagination: PaginationObject): Promise<object[]> {
    throw new Error("Method not implemented.");
  }
  getById(id: string): Promise<object | null> {
    throw new Error("Method not implemented.");
  }
  add(body: object): Promise<object> {
    throw new Error("Method not implemented.");
  }
  update(id: string, body: object): Promise<object> {
    throw new Error("Method not implemented.");
  }
  delete(id: string): Promise<null> {
    throw new Error("Method not implemented.");
  }
}
```

### Javascript

A Service in regular javascript is a an object that contains the methods

```javascript
const UsersService = {
  getAll: (params, pagination) => {},
  getById: (id) => {},
  add: (body) => {},
  update: (id, body) => {},
  delete: (id) => {},
};
```

## What Is A Schema ?

A schema is a way to validate the requests before passing them to your route , that means that each call to your service is a safe and sanitized request

### Schema Structure

The schema structure is very close to the regular schema-validation libraries , each key represents a key in the object and the value have multiple attributes

1. **type**: that can be "string" | "number" | "boolean"
2. **isRequired**
3. **requiredAt**: that is an array that can have the values create | update , this will allow a value to be required in create operation but not in update operation for example
4. **min**
5. **max**
6. **trim**: weather to trim tha value or not
7. **transformer**: a method that will be called at the end of the validation process to perform a custom transformation for the value
8. **customValidator**: a method that will be called after the basic validation , you can check for unique integrity in here or anything else , this method should return an object that contains {result: boolean ; error?: string}

```javascript
[key: string]: {
    type: "string" | "number" | "boolean";
    isRequired?: boolean;
    requiredAt?: Array<"create" | "update">;
    min?: number;
    max?: number;
    trim?: boolean;
    transformer?: (value: any) => Promise<any>;
    customValidator?: (
      value: any,
    ) => { result: boolean; error?: string } | Promise<{ result: boolean; error?: string }>;
  };
```

### Schema example

```javascript
const schema = {
  id: {
    type: "string",
    isRequired: true,
    requiredAt: ["create"],
  },
  username: {
    type: "string",
    isRequired: true,
    requiredAt: ["create"],
    trim: true,
    transformer: (value) => `@${value}`,
    customValidator: async (value) => {
      // Check for unique username
      // return {result : true}
      return { result: false, error: "Username should be unique" };
    },
  },
  rank: {
    type: "number",
    isRequired: true,
    requiredAt: ["update"],
    min: 0,
    max: 1000,
  },
  address: {
    type: "string",
    isRequired: false,
  },
  isValid: {
    type: "boolean",
  },
};
```

## What Are The Configurations

Configurations is an optional parameter passed as the second object while initializing the express-restful instance allows you to change some of the defaults in the library

### What Is The Configuration Object Scheme

1. **defaultFetchLimit**: That default limit that will be passed to the **getAll** route when there is no limit specified
2. **offsetKey**: The offset key that the user will pass to the **/** route
3. **limitKey**: The limit key that the user will pass to the **/** route
4. **responseTransformer**: is a method that will receive every response to transform it , like wrapping the result in a **data** key
5. **localization**: is an object that let you specify the validation error messages

```typescript
defaultFetchLimit: number;
offsetKey: string;
limitKey: string;
responseTransformer: (response: object | Array<object> | null | boolean) => object;
localization: ValidationLocalization;
```

### What Is The localization Object ?

An object that each key is a method that will receive the required information about the error to return the correct message

```typescript
ValidationLocalization = {
  requiredMessage: (key: string) => string;
  typeMismatchMessage: (key: string, value: any, currentType: string, targetType: string) => string;
  minViolationError: (key: string, value: any, min: number) => string;
  maxViolationError: (key: string, value: any, max: number) => string;
```

### The Default Configurations

```typescript
defaultConfigurations = {
  defaultFetchLimit: 15,
  limitKey: "limit",
  offsetKey: "offset",
  localization: {
    requiredMessage: (key) => `"${key}" is required`,
    typeMismatchMessage: (key, value, currentType, targetType) =>
      `"${key}" should have the type of ${targetType} but ${currentType} found`,
    minViolationError: (key, value, min) => `"${key}" should have be at least ${min}`,
    maxViolationError: (key, value, max) => `"${key}" should have the at max ${max}`,
  },
  responseTransformer: (response) => ({
    data: response,
    success: true,
  }),
};
```

## Adding a route

express-restful instance have an **add** method that will take the url , schema , service

### Route middlewares

The **add** method accepts the third parameter to be an object of middlewares to allow the users to specify an auth middleware for example or a role middleware for each spesific operation

```typescript
 middlewares: Partial<{ get: any; post: any; put: any; delete: any }>
```

## Full Example

```javascript
import * as UsersService from "../services/users";
import express from "express";
import ExpressRestful from "express-restful";

const app = express();

const usersSchema = {
  id: {
    type: "string",
    isRequired: true,
    requiredAt: ["create"],
  },
  username: {
    type: "string",
    isRequired: true,
    requiredAt: ["create"],
    trim: true,
    transformer: (value) => `@${value}`,
    customValidator: async (value) => {
      // Check for unique username
      // return {result : true}
      return { result: false, error: "Username should be unique" };
    },
  },
  rank: {
    type: "number",
    isRequired: true,
    requiredAt: ["update"],
    min: 0,
    max: 1000,
  },
  address: {
    type: "string",
    isRequired: false,
  },
  isValid: {
    type: "boolean",
  },
};

const usersService = {
  getAll: (params, pagination) => {},
  getById: (id) => {},
  add: (body) => {},
  update: (id, body) => {},
  delete: (id) => {},
};

const middlewares = {};

const expressRestful = ExpressRestful(app, {});
expressRestful.add("/users/", usersSchema, usersService, middlewares);

app.listen(3000);
```
