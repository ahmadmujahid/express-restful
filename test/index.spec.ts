import supertest from "supertest";
import app from "./express";
import ExpressRestful from "../src";
import UsersService from "./express/users";

const expressRestful = ExpressRestful(app);
const requests = supertest(app);
const userService = new UsersService();

describe("API Testing", () => {
  it("Should not fail", async (done) => {
    expect((await requests.get("/healthCheck")).status).toBe(200);
    done();
  });

  it("Should add a route", async (done) => {
    expressRestful.add(
      "/users",
      {
        name: { type: "string", requiredAt: ["create"], isRequired: true },
      },
      userService,
    );
    done();
  });
});
