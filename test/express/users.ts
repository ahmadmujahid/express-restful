import { PaginationObject, Service } from "../../src";

export default class UsersService implements Service {
  users: any[] = [];
  getAll(params: object, pagination: PaginationObject): object[] | Promise<object[]> {
    return this.users;
  }
  getById(id: string): object | Promise<object | null> | null {
    return this.users.find((item: any) => item.id === id) || null;
  }
  add(body: object): object | Promise<object> {
    const user = { ...body, id: this.users.length };
    this.users.push(user);
    return user;
  }

  checkUserExists(id: string): boolean {
    return !!this.users.find((item: any) => item.id === id);
  }

  update(id: string, body: object): object | Promise<object> {
    const currentIndex = this.users.findIndex((item: any) => item.id === id);
    this.users[currentIndex] = { ...this.users[currentIndex], ...body };
    return this.users[currentIndex];
  }

  delete(id: string): Promise<null> | null | void {
    this.users.filter((item: any) => item.id !== id);
  }
}
