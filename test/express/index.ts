import express from "express";

const app = express();

app.use("/healthCheck", (req, res) => {
  res.status(200);
  res.send();
});

app.listen(3000, () => {
  console.log("Connected to port 3000");
});

export default app;
